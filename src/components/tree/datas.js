export default {
    name: '트리',
    children: [
        { name: '1' },
        { name: '2' },
        {
            name: '자식트리',
            children: [
                {
                    name: '자식트리2',
                    children: [
                        { name: '3' },
                        { name: '4' }
                    ]
                },
                { name: '5' },
                { name: '6' },
                {
                    name: '자식트리3',
                    children: [
                        { name: '7' },
                        { name: '8' }
                    ]
                }
            ]
        }
    ]
}