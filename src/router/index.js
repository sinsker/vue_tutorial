import Vue from 'vue'
import Router from 'vue-router'

import HelloWorld from '@/components/HelloWorld'
import Basic from '@/components/example/Basic'
import BasicBind from '@/components/example/BasicBind'
import ModelAndEvent from '@/components/example/ModelAndEvent'
import GridApp from '@/components/grid/GridApp'
import TreeApp from '@/components/tree/TreeApp'

Vue.use(Router);

export default new Router({
  routes: [
    {
        path: '/',
        name: '메인화면',
        component: HelloWorld
    },
    {
        path: '/basic',
        name: '기본문법',
        component: Basic
    },
    {
        path: '/basicbind',
        name: '기본문법2',
        component: BasicBind
    },
    {
        path: '/modelAndEvent',
        name : '멀티바인딩 및 이벤트',
        component : ModelAndEvent

    },
    {
        path: '/grid',
        name : '그리드앱 데모',
        component : GridApp

    },
    {
        path:'/tree',
        name : '트리 구조',
        component : TreeApp
    }
  ]
})
